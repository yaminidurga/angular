import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 initialCount = 10;
  countChange(event) {
    this.initialCount = event;
  }
}



